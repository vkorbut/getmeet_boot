$(function(){
	/// is Mobile
	var isMobile = {
		 Android: function() {
			  return navigator.userAgent.match(/Android/i);
		 },
		 BlackBerry: function() {
			  return navigator.userAgent.match(/BlackBerry/i);
		 },
		 iOS: function() {
			  return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		 },
		 Opera: function() {
			  return navigator.userAgent.match(/Opera Mini/i);
		 },
		 Windows: function() {
			  return navigator.userAgent.match(/IEMobile/i);
		 },
		 any: function() {
			  return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		 }
	};
	if(isMobile.any()){
		$('body').addClass('is-mobile')
		$('body').append('<div class="is-mobile-wr"><div class="inner"><div><div class="mob-wr"><img src="img/logo-mob.png" srcset="img/logo-mob@2x.png 2x" alt="" class="logo-mob"><span class="ttl">Будьте всегда на связи с нашим мобильным приложением</span><p>Установите наше прилажение на вашсмартфон, создавайте события, принимайтеучастие в жизни вашего города,знакомьтесь с новыми людьми и общайтесьбез границ</p><div class="download"><a href="#"><img src="img/appstore-wh.png", srcset="img/appstore-wh@2x.png 2x" alt=""></a><a href="#"><img src="img/googleplay-wh.png", srcset="img/googleplay-wh@2x.png 2x" alt=""></a></div></div></div></div></div>');
	}

	
	/// menu active point
	$('.user-menu .act-area > span').each(function(){
		var actPos = $('.user-menu .act').offset().top - $('.user-menu').offset().top
		$(this).css({
			top: actPos-5+'px'
		});
	});
	
	
	
	/// time text blow in event card
	$('.ev-item .details').each(function(){
		$(this).css({
			marginRight: $(this).parent().find('.users').width()+'px'
		});
	});
	
	/// select events
	function selectDropSize() {
		$('.select .dropdown').each(function(){
			var drop = $(this);
			var dropPosTop = drop.offset().top
			var windowScroll = $(window).scrollTop()
			var dropPos = dropPosTop-windowScroll
			var wh = $(window).height()
			var dropH = wh-dropPos
			if ($(this).find('ul').height()>dropH) {
				$(this).css({
					maxHeight: dropH-14+'px'
				});
			}
			else {
				$(this).css({
					maxHeight: 'none'
				});
			}
		})
		
	}
	$(window).on('load scroll resize', function(){
		selectDropSize()
	}); 
	
	
	$('.select .select-txt').on('click', function(){
		var select = $(this).parents('.select')
		$('.select').removeClass('open');
		if (select.hasClass('open')) {
			select.removeClass('open');
		}
		else {
			select.addClass('open')
		}
//		select.toggleClass('open');
		selectDropSize()
		return false
	});
	
	$('.ev-select li').on('click', function(){
		var val = $(this).text()
		var itClass = $(this).attr('class');
		$('.ev-select li').removeClass('act');
		$(this).addClass('act');
		$('#event-filter-select').each(function(){
			$(this).val(val).change()
			$(this).parent()
				.find('.select-txt')
				.text(val)
				.removeClass('ev-type01 ev-type02 ev-type03 ev-type04')
				.addClass(itClass)
				.removeClass('act');
		});
		dropDownClose();
	});
	$('.select .dropdown li:first-child').each(function(){
		$(this).addClass('act');
	});
	$('.select .dropdown li').on('click', function(){
		var val = $(this).text();
		var select = $(this).parents('.select').find('select');
		var selectTxt = $(this).parents('.select').find('.select-txt');
		$(select).val(val).change();
		$(selectTxt).text(val);
		$(this).parents('.select').removeClass('open');
		$('.select .dropdown li').removeClass('act');
		$(this).addClass('act');
	});
	
	
	/// dropdown events
	function dropDownOpen() {
		var html = $(this).html();
		var posX = $(this).offset().left
		var posY = $(this).offset().top
		if($('.dropdown-body').hasClass('act')) {
			$('.dropdown-body, .dropdown, .dropd-lnk, .select, .date-one-wr').removeClass('act open');
			$('body').removeClass('body-fix');
		}
		else {
			$('.dropdown-body').toggleClass('act');
			$('body').toggleClass('body-fix');
			$('.dropdown').removeClass('act');
			$('#'+$(this).data('dropdown')).addClass('act');

			$(this).toggleClass('act');
			if ($(this).hasClass('f-toggle')) {
				$('#'+$(this).data('dropdown')).addClass('act').css({
					left: (posX-676)+'px',
					top: posY+68+'px'
				});
			}
			if ($(this).hasClass('cal-toggle')) {
				$('#'+$(this).data('dropdown')).addClass('act').css({
					left: (posX-118)+'px',
					top: posY+68+'px'
				});
			}
			if ($(this).hasClass('t-toggle')) {
				$('#'+$(this).data('dropdown')).addClass('act').css({
					left: posX+'px',
					top: posY+68+'px'
				});
			}
			dropdScr();
			return false
		}
	}
	function dropDownClose() {
		$('.dropdown-body, .dropdown, .dropd-lnk, .select, date-one-wr').removeClass('act open');
		$('body').removeClass('body-fix');
	}
   $('.dropd-lnk').on('click', dropDownOpen);
	$('.dropdown .close').on('click', dropDownClose);
	
	$(document).on('click', function(event){
		if($(event.target).closest('.dropdown, .ui-corner-all').length) 
		return;
		dropDownClose()
		event.stopPropagation();
	});

	
	// event types toggle
	$('#ev-type-filter .item').each(function(){
		if($(this).hasClass('act')) {
			$(this).find('.checkbox input').prop('checked', true);
			$(this).find('.item-ttl').addClass('act');
		}
	})
	$('#ev-type-filter .item-ttl').on('click', function(){
		var item = $(this).parents('.item');
		$(this).toggleClass('act');
		item.toggleClass('act');
		if (item.hasClass('act')) {
			item.find('.checkbox input').prop('checked', true);
		}
		else {
			item.find('.checkbox input').prop('checked', false);
		}
	});
	$('#ev-type-filter .checkbox input').on('change', function(){
		var item = $(this).parents('.item');
		var itemTitle = item.find('.item-ttl');
		var chbxs = item.find('input[type="checkbox"]:checked').length
		if (chbxs == 0) {
			item.removeClass('act');
			itemTitle.removeClass('act');
		}
		else {
			item.addClass('act');
			itemTitle.addClass('act');
		}
	});
	$('#filt-reset').on('click', function(){
		$('.ev-types-wr .item, .ev-types-wr .item-ttl').addClass('act');
		$('.ev-types-wr .checkbox input').prop('checked', true);
	});
	$('#ev-type-switch .item-ttl').on('click', function(){
		var item = $(this).parents('.item');
		$('#ev-type-switch .item-ttl').removeClass('act');
		$(this).addClass('act');
		$('#main-img').attr('src', item.data('img'));
		$('.ph-upl-wr').removeClass('clear').removeClass('clear');
		$('.ph-upl-wr').removeClass('clear').removeClass('clear');
		$('.place-choice').removeAttr('disabled');
		$('.place-choice').removeClass('type1 type2 type3 type4');
		if ($(this).hasClass('it1')) {
			$('.place-choice').addClass('type1')
		}
		if ($(this).hasClass('it2')) {
			$('.place-choice').addClass('type2')
		}
		if ($(this).hasClass('it3')) {
			$('.place-choice').addClass('type3')
		}
		if ($(this).hasClass('it4')) {
			$('.place-choice').addClass('type4')
		}
		return false
	});
	
	$('.place-choice').on('click', function(){
		$('.place-selection').removeAttr('style');
		$('.date-one-wr').removeClass('act');
		if($(this).hasClass('type1')) {
			$('.place-selection .map-marker').attr('src','img/map-marker1.png');
			$('.place-selection .map-overlay').attr('src', 'img/map-overlay1.png');
			$('.place-selection .map-overlay').attr('srcset', 'img/map-overlay1@2x.png 2x');
		}
		if($(this).hasClass('type2')) {
			$('.place-selection .map-marker').attr('src','img/map-marker2.png');
			$('.place-selection .map-overlay').attr('src', 'img/map-overlay2.png');
			$('.place-selection .map-overlay').attr('srcset', 'img/map-overlay2@2x.png 2x');
		}
		if($(this).hasClass('type3')) {
			$('.place-selection .map-marker').attr('src','img/map-marker3.png');
			$('.place-selection .map-overlay').attr('src', 'img/map-overlay3.png');
			$('.place-selection .map-overlay').attr('srcset', 'img/map-overlay3@2x.png 2x');
		}
		if($(this).hasClass('type4')) {
			$('.place-selection .map-marker').attr('src','img/map-marker4.png');
			$('.place-selection .map-overlay').attr('src', 'img/map-overlay4.png');
			$('.place-selection .map-overlay').attr('srcset', 'img/map-overlay4@2x.png 2x');
		}
	});
	
	
	// photo upload
	function readURL(input) {
	  if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#main-img').attr('src', e.target.result);
			$('.ph-upl-wr').removeClass('clear');
		}
		reader.readAsDataURL(input.files[0]);
	  }
    }
    
	$(".ph-upl-wr input").change(function(){
	  readURL(this);
	});
	
	
	
	// datepicker
	
	$.datepicker.regional['ru'] = {
		closeText: 'Закрыть',
		prevText: '&#x3c;Пред',
		nextText: 'След&#x3e;',
		currentText: 'Сегодня',
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
		'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		monthNamesShort: ['Января','Февраля','Марта','Апреля','Мая','Июня',
		'Июля','Августа','Сентября','Октября','Ноября','Декабря'],
		dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
		dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
		dayNamesMin: ['вс','пн','вт','ср','чт','пт','сб'],
		weekHeader: 'Нед',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['ru']);
	
	$('.datepicker').datepicker({
		range: 'period',
		numberOfMonths: 1,
		onSelect: function(dateText, inst, extensionRange) {
			$('[name=startDate]').val(extensionRange.startDateText);
			$('[name=endDate]').val(extensionRange.endDateText);
		}
	});

	$('.date-one-wr .picker').datepicker({
		numberOfMonths: 1,
		dateFormat: 'd M',
		onSelect: function(date, picker) {
			$('[name=picker-date]').val(date+', '+$('.time-range-one').prop('value'));
		}
	});
	
	$('.date-one-wr .save').on('click', function(){
		$('[name=picker-date]').val($('.date-one-wr .picker').val()+', '+$('.time-range-one').prop('value'));
		$('.date-one-wr').removeClass('act');
	});
	
	$('.date-one-wr .cancel').on('click', function(){
		$('.date-one').val('');
		$('.date-one-wr').removeClass('act');
	});
	
	$('.date-one').on('focus click', function(){
		$('.date-one-wr').addClass('act');
		return false
	});
	
	birthpicker = $('.date-one-wr .birth-picker')
	birthpicker.datepicker({
		numberOfMonths: 1,
		dateFormat: 'd M',
		changeYear: false,
		titleFormat: 'M',
		onSelect: function(date, picker) {
			$('[name=birth-date]').val(date+' '+$('#birth-year').prop('value'));
		}
	});
	$('#birth-year').on('change', function(date, picker){
		$('[name=birth-date]').val($(this).val()+' '+$(this).prop('value'));
	})
	
	
	
	$(document).on('click', function(event){
		if($(event.target).closest('.date-one-wr, .ui-corner-all').length) 
		return;
		$('.date-one-wr').removeClass('act');
		event.stopPropagation();
	});
	
	
	// number require
	$('.inp-num').keypress(function(key) {
		if ((key.charCode < 48 || key.charCode > 57 )&& (key.charCode != 32) && (key.charCode != 0) ) return false;
	});
	
	
	$('[name="ev-pay-type"]').on('change', function(){
		if ($('#pay-true').is(':checked')) {
			$('#event-cost-input').prop('disabled', false);
		}
		else {
			$('#event-cost-input').prop('disabled', true).val('');
		}
	});
	
			
	// textarea resize
	
	$.each($('textarea[data-autoresize]'), function() {
	var offset = this.offsetHeight - this.clientHeight;
	var resizeTextarea = function(el) {
	  $(el).css('height', 'auto').css('height', el.scrollHeight + offset);
	};
	  $(this).on('keyup input', function() { resizeTextarea(this); })
	});
	$('textarea[data-autoresize]').blur(function(){
		var value = $(this).val().length
		if (value<1) {
			$(this).removeAttr('style');
		}
	});
	
	
	
	
	// owl slider
	$('.ev-list-slider .slider').owlCarousel({
		items: 4,
		margin: 12,
		dots: false,
		nav: true,
		responsive: {
			0: {
				items: 1
			},
			1000: {
				items: 3
			},
			1398: {
				items: 4
			}
		}
	});
	$('.user-gallery .slider').owlCarousel({
		items: 5,
		margin: 0,
		dots: false,
		nav: true
	});
	
	// fancybox
	var fancybox =
	$('.fancybox').fancybox({
		tpl: {
			next: '<a title="" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
			prev: '<a title="" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>',
			closeBtn : '<span class="fancybox-close"></span>'
		},
		titlePosition: 'over',
		padding: 0,
		helpers : {
		   media : {}
		},
		openEffect : 'none',
		closeEffect : 'none',
		prevEffect : 'none',
		nextEffect : 'none'
	});
	
	
	

	
	
	// ranger
	$('.time-range').ionRangeSlider({
		values: [
            "00:00","00:30","01:00","01:30", "02:00","02:30","03:00","03:30", "04:00","04:30","05:00","05:30", "06:00","06:30","07:00","07:30", "08:00","08:30","09:00","09:30", "10:00","10:30","11:00","11:30", "12:00","12:30","13:00","13:30", "14:00","14:30","15:00","15:30",
				"14:00","14:30","15:00","15:30",
				"16:00","16:30","17:00","17:30",
				"18:00","18:30","19:00","19:30",
				"20:00","21:30","22:00","22:30",
				"23:00","23:30","24:00"
        ],
		grid: false,
		type: 'double',
		force_edges: false,
		prefix: false
	});
	$('.time-range-one').ionRangeSlider({
		values: [
            "00:00","00:30","01:00","01:30", "02:00","02:30","03:00","03:30", "04:00","04:30","05:00","05:30", "06:00","06:30","07:00","07:30", "08:00","08:30","09:00","09:30", "10:00","10:30","11:00","11:30", "12:00","12:30","13:00","13:30", "14:00","14:30","15:00","15:30",
				"14:00","14:30","15:00","15:30",
				"16:00","16:30","17:00","17:30",
				"18:00","18:30","19:00","19:30",
				"20:00","21:30","22:00","22:30",
				"23:00","23:30","24:00"
        ],
		grid: false,
		type: 'single',
		force_edges: false,
		prefix: false,
		from: 24,
	});
	$('.age-range').ionRangeSlider({
		min: 16,
		max: 99,
		grid: false,
		type: 'double',
		force_edges: false,
		prefix: false
	});
	sexRange = $('.sex-range');
	
	sexRange.ionRangeSlider({
		min: 0,
		max: 100,
		from: 50,
		grid: false,
		force_edges: false,
		prefix: false,
		postfix: ' / '+sexRange.data("wm"),
		onChange: function() {
			sexRangeVal = sexRange.prop("value");
			sexRangeValP = 100-sexRangeVal
			$('.sex-range-wr').find('.irs-single').html(sexRangeVal+' / '+sexRangeValP);
		},
		onStart:function() {
			sexRangeVal = sexRange.prop("value");
			sexRangeValP = 100-sexRangeVal
			$('.sex-range-wr').find('.irs-single').html(sexRangeVal+' / '+sexRangeValP);
		},
		onFinish:function() {
			sexRangeVal = sexRange.prop("value");
			sexRangeValP = 100-sexRangeVal
			$('.sex-range-wr').find('.irs-single').html(sexRangeVal+' / '+sexRangeValP);
		}
	});
	function sexRangePercent() {
		sexRangeVal = sexRange.prop("value");
		console.log("Value: " + sexRangeVal);
		$('.sex-range-wr').find('.irs-single').html('sdcds');
	}
//	sexRangeDate = sexRange.data("ionRangeSlider");
//	sexRange.on('change', function(){
//		sexRangeVal = sexRange.prop("value");
//		console.log("Value: " + sexRangeVal);
//		$('.sex-range-wr').find('.irs-single').html('sdcds');
//	});
	
	
	// anchor scroll
	$('.anch-lnk').on('click', function(){
		var blockId = $(this).attr('href');
		$('html, body').stop().animate({
			scrollTop: $(blockId).offset().top
		},  700);
		return false;
	});
	
});
function menuHeight() {
	if ($(window).height() <= 740) {
		$('.menu-col').addClass('scroll');
	}
	else {
		$('.menu-col').removeClass('scroll');
	}
}
function dropdScr() {
	$('.dropdown.act:not(.fix-height)').each(function(){
		var itemH = $(this).outerHeight()
		var itemPos = $(this).offset().top
		var h = itemH + itemPos
		if ($(window).height()<h+10) {
			$(this).addClass('scroll');
		}
		if ($(window).height()>h+10) {
			$(this).removeClass('scroll');
		}
	});
	$('.dropdown.fix-height').each(function(){
		if ($(window).height() < 650) {
			$(this).addClass('static');
		}
		else {
			$(this).removeClass('static');
		}
	});
}
function chatWidth() {
	wSum = $('.menu-col').width() + $('.chats-list').width() + 803
	if ($(window).width() < wSum) {
		$('.chat-wr').addClass('w-auto');
	}
	else {
		$('.chat-wr').removeClass('w-auto');
	}
}
$(window).on('load resize', function(){
	menuHeight()
	chatWidth()
	dropdScr()
//	setTimeout(function(){
//		
//	},100);
});