function initMap() {
		
			var secheltLoc = new google.maps.LatLng(53.9093283,27.5617939,13);
			var myMapOptions = {
				 zoom: 15
				,center: secheltLoc
				,mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var theMap = new google.maps.Map(document.getElementById("map"), myMapOptions);
			var marker = new google.maps.Marker({
				map: theMap,
				draggable: false,
				position: secheltLoc,
				visible: true,
				icon: {
					url: "img/map-marker1.png",
					scaledSize: new google.maps.Size(64, 53)
				}
			});
			var boxText = document.createElement("div");
			boxText.style.cssText = "";
			boxText.innerHTML = '<a href="item.html" class="ev-item min map-baloon" style="font-family: effraregular" onclick=""><span class="img"><img src="img/tmp/event-pic-min1.jpg" alt=""><span class="dist">0.4 км</span><span class="type t01"></span></span><span class="ev-info"><span class="ttl ta-l">Пляжный футбол</span><span class="details ta-l"><span class="price">500<span class="rub">i</span></span><span class="date">21 июня, 18:00</span></span></span></a>';
			var myOptions = {
				 content: boxText
				,disableAutoPan: false
				,maxWidth: 0
				,pixelOffset: new google.maps.Size(-136, -204)
				,zIndex: null
				,boxStyle: {
				  opacity: 1
				  ,width: "250px"
				 }
				,contextmenu: true
				,closeBoxMargin: "0"
				,closeBoxURL: "img/map-close.png"
				,infoBoxClearance: new google.maps.Size(1, 1)
				,isHidden: false
				,pane: "floatPane"
				,enableEventPropagation: false
			};
			google.maps.event.addListener(marker, "click", function (e) {
				ib.open(theMap, this);
			});
			var ib = new InfoBox(myOptions);
			ib.close(theMap, marker);

	}
